#include <iostream>
#include <fcntl.h>
#include <iomanip>
#include <unistd.h>
#include <bitset>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
using namespace std;

void createSimFile(vector <string> instructions, vector <int> data, int dataStartAddress, char* filename);
string trimStr(string str);

int main(int argC, char* argv[]) //argv[2] = input filename, argv[4] = output filename
{
    char buffer[4];
    int i;
    char* iPtr;
    iPtr = (char*)(void*)&i;

    bool valid;
    bool hitBreak = false;
    stringstream stream;
    stringstream instructionStream;
    vector <string> instruction;
    vector <int> data;
    int dataStartAddress;
    char* inFilename = "test2.bin";
    char* disFilename = "tempDis.txt";
    char* simFilename = "tempSim.txt";
    if (argc == 5)
    {
        disFilename = (string)argv[4] + "_dis.txt";
        simFilename = (string)argv[4] + "_sim.txt";
        inFilename = (string)argv[2];
    }



    ofstream disOut(disFilename);

    int address = 96;



    //Binary 0 and 1
    string str; //full instruction
    string op;
    string rs;
    string rt;
    string rd;
    string sa;
    string func;
    string imm;
    string idx;


    //for output
    string operation;
    string rsReg;
    string rtReg;
    string rdReg;
    string shiftAmount;
    string immediate;
    string bimmediate;
    string index;


    int FD = open(inFilename, O_RDONLY);

    int amt = 4;
    while (amt != 0)
    {
        amt = read(FD, buffer, 4);
        if (amt == 4)
        {
            iPtr[0] = buffer[3];
            iPtr[1] = buffer[2];
            iPtr[2] = buffer[1];
            iPtr[3] = buffer[0];

            str = bitset<32>(i).to_string();

            if (!hitBreak)
            {
                op = str.substr(1, 5);
                rs = str.substr(6, 5);
                rt = str.substr(11, 5);
                rd = str.substr(16, 5);
                sa = str.substr(21, 5);
                func = str.substr(26, 6);

                imm = str.substr(16, 16);
                idx = str.substr(6, 26);

                if (str[0] == '1')
                    valid = true;
                else
                    valid = false;

                //rs
                {
                    bitset<5>  x(rs);
                    stream << x.to_ulong();
                    rsReg = "R" + stream.str();
                    stream.str("");
                }

                //rd
                {
                    bitset<5>  x(rd);
                    stream << x.to_ulong();
                    rdReg = "R" + stream.str();
                    stream.str("");
                }

                //rt
                {
                    bitset<5> x(rt);
                    stream << x.to_ulong();
                    rtReg = "R" + stream.str();
                    stream.str("");
                }

                //sa
                {
                    bitset<5>  x(sa);
                    stream << x.to_ulong();
                    shiftAmount = "#" + stream.str();
                    stream.str("");
                }

                //imm
                {
                    // bitset<16>  x(imm);
                    int y = ((i << 16) >> 16);
                    stream << y;
                    immediate = stream.str();
                    stream.str("");
                }

                //bimm
                {
                    string tempstr = imm + "00";
                    bitset<18>  x(tempstr);
                    stream << x.to_ulong();
                    bimmediate = stream.str();
                    stream.str("");
                }

                //idx
                {
                    string tempstr = idx + "00";
                    bitset<28>  x(tempstr);
                    stream << x.to_ulong();
                    index = stream.str();
                    stream.str("");
                }

                //Print bit string
                disOut << str[0] << " " << op << " " << rs << " " << rt << " " << rd << " " << sa << " " << func << "\t"; //Output to disassembly file


                if (!valid) // invalid
                {
                    instructionStream << address << "\t\t";
                    instructionStream << "Invalid Instruction" << endl;
                    instruction.push_back(instructionStream.str());
                }
                else {
                    instructionStream << address << "\t\t";

                    if (str == "10000000000000000000000000000000")
                        instructionStream << "NOP" << endl;

                    //op code 0
                    else if (op == "00000") {
                        if (func == "100000") //ADD
                            instructionStream << "ADD\t\t" << rdReg << ", " << rsReg << ", " << rtReg << endl;

                        else if (func == "001000") //JR
                            instructionStream << "JR\t\t" << rsReg << endl;

                        else if (func == "100100") //AND
                            instructionStream << "AND\t\t" << rdReg << ", " << rsReg << ", " << rtReg << endl;

                        else if (func == "100101") //OR
                            instructionStream << "OR\t\t" << rdReg << ", " << rsReg << ", " << rtReg << endl;

                        else if (func == "100010") // SUB
                            instructionStream << "SUB\t\t" << rdReg << ", " << rsReg << ", " << rtReg << endl;

                        else if (func == "000000") //SLL                            
                            instructionStream << "SLL\t\t" << rdReg << ", " << rtReg << ", " << shiftAmount << endl;

                        else if (func == "000010") //SRL
                            instructionStream << "SRL\t\t" << rdReg << ", " << rtReg << ", " << shiftAmount << endl;

                        else if (func == "001010") // MOVZ
                            instructionStream << "MOVZ\t" << rdReg << ", " << rsReg << ", " << rtReg << endl;

                        else if (func == "001101") {// BREAK
                            instructionStream << "BREAK" << endl;
                            hitBreak = true;
                            dataStartAddress = address + 4; //Adress where memory data begin
                        }

                    }
                    //other op codes                
                    else if (op == "11100") // MUL
                        instructionStream << "MUL\t\t" << rdReg << ", " << rsReg << ", " << rtReg << endl;

                    else if (op == "01000") // ADDI
                        instructionStream << "ADDI\t" << rtReg << ", " << rsReg << ", #" << immediate << endl;

                    else if (op == "01011") // SW
                        instructionStream << "SW\t\t" << rtReg << ", " << immediate << "(" << rsReg << ")" << endl;

                    else if (op == "00011") // LW
                        instructionStream << "LW\t\t" << rtReg << ", " << immediate << "(" << rsReg << ")" << endl;

                    else if (op == "00001") // BLTZ
                        instructionStream << "BLTZ\t" << rsReg << ", #" << bimmediate << endl;

                    else if (op == "00010") // Jump
                        instructionStream << "J\t\t#" << index << endl;

                    else if (op == "00100") // BEQ
                        instructionStream << "BEQ\t\t" << rsReg << ", " << rtReg << ", #" << bimmediate << endl;

                    else
                        instructionStream << "Not yet implemented" << endl; //Catch unknown operations

                    instruction.push_back(instructionStream.str());

                }
                disOut << instructionStream.str(); //Output to disassembly file
                instructionStream.str("");
            }
            else {
                data.push_back(i);
                disOut << str << "\t\t" << address << "\t\t" << i << endl; //Output to disassembly file
            }
            address += 4;
        }
    }


    createSimFile(instruction, data, dataStartAddress, simFilename);

    return 0;
}





void createSimFile(vector <string> instructions, vector <int> data, int dataStartAddress, char* filename)
{
    string instrAddr;
    string instrName;
    string instr1;
    string instr2;
    string instr3;
    stringstream stream;

    int cycle = 1;
    int registers[32] = { 0 };
    ofstream fout;
    fout.open(filename);
    bool foundBreak = false;



    if (fout.fail())
    {
        cout << "Could not create file." << endl;
        exit(1);
    }

    for (int i = 0; i < instructions.size() || !foundBreak; ++i)
    {
        if (instructions[i].find("BREAK") == string::npos)
        {
            foundBreak = true;
        }

        if (instructions[i].find("Invalid") == string::npos) //If valid instruction
        {
            fout << "====================" << endl;
            fout << "cycle:" << cycle << "\t" << instructions[i] << endl;

            //Perform instructions
            stream << instructions[i];
            getline(stream, instrAddr, '\t');
            getline(stream, instrName, '\t');
            getline(stream, instrName, '\t');

            instrAddr = trimStr(instrAddr);

            cout << "Address: " << instrAddr << ", Instruction: " << instrName << endl;

            //Determine instruction name

            if (instrName == "J") //Jump
            {
                getline(stream, instr1);
                instr1 = trimStr(instr1);

                i = ((stoi(instr1) - 100) / 4);
            }
            else if (instrName == "JR") //Jump Register
            {
                getline(stream, instr1);
                instr1 = trimStr(instr1);

                int x = registers[stoi(instr1)];

                i = ((registers[stoi(instr1)]) - 100) / 4;
            }
            else if (instrName == "BEQ") //Branch on Equal
            {
                getline(stream, instr1, ',');
                getline(stream, instr2, ',');
                getline(stream, instr3);

                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);
                instr3 = trimStr(instr3);

                if (registers[stoi(instr1)] == registers[stoi(instr2)])
                    i += (stoi(instr3) / 4);
            }
            else if (instrName == "BLTZ") //Branch on Less Than 0
            {
                getline(stream, instr1, ',');
                getline(stream, instr2);


                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);

                if (registers[stoi(instr1)] < 0)
                    i += (stoi(instr2) / 4);
            }
            else if (instrName == "ADD") //Add
            {
                getline(stream, instr1, ',');
                getline(stream, instr2, ',');
                getline(stream, instr3);

                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);
                instr3 = trimStr(instr3);

                registers[stoi(instr1)] = registers[stoi(instr2)] + registers[stoi(instr3)];
            }
            else if (instrName == "ADDI") //Add immediate
            {
                getline(stream, instr1, ',');
                getline(stream, instr2, ',');
                getline(stream, instr3);

                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);
                instr3 = trimStr(instr3);

                registers[stoi(instr1)] = registers[stoi(instr2)] + stoi(instr3);
            }
            else if (instrName == "SUB") //Subtract
            {
                getline(stream, instr1, ',');
                getline(stream, instr2, ',');
                getline(stream, instr3);

                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);
                instr3 = trimStr(instr3);

                registers[stoi(instr1)] = registers[stoi(instr2)] - registers[stoi(instr3)];
            }
            else if (instrName == "SW") //Store Word
            {
                getline(stream, instr1, ',');
                getline(stream, instr2, '(');
                getline(stream, instr3);

                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);
                instr3 = trimStr(instr3);

                data[((stoi(instr2) + registers[stoi(instr3)]) - dataStartAddress) / 4] = registers[stoi(instr1)];
            }
            else if (instrName == "LW") //Load Word
            {
                getline(stream, instr1, ',');
                getline(stream, instr2, '(');
                getline(stream, instr3);

                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);
                instr3 = trimStr(instr3);

                registers[stoi(instr1)] = data[((stoi(instr2) + registers[stoi(instr3)]) - dataStartAddress) / 4];
            }
            else if (instrName == "SLL") //Shift left logical
            {
                getline(stream, instr1, ',');
                getline(stream, instr2, ',');
                getline(stream, instr3);

                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);
                instr3 = trimStr(instr3);

                int z = registers[stoi(instr2)];
                z = z << 2;

                registers[stoi(instr1)] = z;
            }
            else if (instrName == "SRL") //Shift right logical
            {
                getline(stream, instr1, ',');
                getline(stream, instr2, ',');
                getline(stream, instr3);

                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);
                instr3 = trimStr(instr3);

                int z = registers[stoi(instr2)];
                z = z >> 2;

                registers[stoi(instr1)] = z;
            }
            else if (instrName == "MUL") //Multiply
            {
                getline(stream, instr1, ',');
                getline(stream, instr2, ',');
                getline(stream, instr3);

                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);
                instr3 = trimStr(instr3);

                registers[stoi(instr1)] = registers[stoi(instr2)] * registers[stoi(instr3)];
            }
            else if (instrName == "AND") //And
            {
                getline(stream, instr1, ',');
                getline(stream, instr2, ',');
                getline(stream, instr3);

                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);
                instr3 = trimStr(instr3);

                registers[stoi(instr1)] = registers[stoi(instr2)] & registers[stoi(instr3)];
            }
            else if (instrName == "OR") //Or
            {
                getline(stream, instr1, ',');
                getline(stream, instr2, ',');
                getline(stream, instr3);

                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);
                instr3 = trimStr(instr3);

                registers[stoi(instr1)] = registers[stoi(instr2)] | registers[stoi(instr3)];
            }
            else if (instrName == "MOVZ") //Subtract
            {
                getline(stream, instr1, ',');
                getline(stream, instr2, ',');
                getline(stream, instr3);

                instr1 = trimStr(instr1);
                instr2 = trimStr(instr2);
                instr3 = trimStr(instr3);

                if (registers[stoi(instr3)] == 0)
                    registers[stoi(instr1)] = registers[stoi(instr2)];
            }


            fout << "registers:" << endl; //Print registers
            fout << "r00:\t" << registers[0] << "\t" << registers[1] << "\t" << registers[2] << "\t" << registers[3] << "\t" << registers[4] << "\t" << registers[5] << "\t" << registers[6] << "\t" << registers[7] << endl;
            fout << "r08:\t" << registers[8] << "\t" << registers[9] << "\t" << registers[10] << "\t" << registers[11] << "\t" << registers[12] << "\t" << registers[13] << "\t" << registers[14] << "\t" << registers[15] << endl;
            fout << "r16:\t" << registers[16] << "\t" << registers[17] << "\t" << registers[18] << "\t" << registers[19] << "\t" << registers[20] << "\t" << registers[21] << "\t" << registers[22] << "\t" << registers[23] << endl;
            fout << "r24:\t" << registers[24] << "\t" << registers[25] << "\t" << registers[26] << "\t" << registers[27] << "\t" << registers[28] << "\t" << registers[29] << "\t" << registers[30] << "\t" << registers[31] << endl << endl;



            fout << "data:" << endl; //Print data
            for (int j = 0; j < data.size(); j++)
            {
                if (j % 8 == 0)
                {
                    fout << dataStartAddress + (4 * j) << ":\t";
                    fout << data[j] << "\t";
                }
                else if ((j + 1) % 8 == 0 && j != data.size())
                {
                    fout << data[j] << endl;
                }
                else
                {
                    fout << data[j] << "\t";
                }

            }
            fout << endl;

            stream.str("");
            cycle++;
        }
    }

    fout.close();
}



string trimStr(string str)
{
    string ret = "";
    for (int i = 0; i < str.size(); i++)
    {
        if (str[i] != '(' && str[i] != ')' && str[i] != '#' && str[i] != '\0' && str[i] != '\t' && str[i] != ' ' && str[i] != 'R')
            ret += str[i];
    }
    return ret;
}